import lombok.NonNull;

import java.util.List;

import static java.lang.Integer.parseInt;

public class Day2 {

    private final List<String> input = FileReader.getStringInput(Day2.class, "day2.txt");
    private final static String FORWARD = "forward";
    private final static String DOWN = "down";
    private final static String UP = "up";


    private int horizontal = 0;
    private int depth = 0;
    private int aim = 0;

    public static void main(final String[] args) {
        final var day2 = new Day2();
        if (day2.partOne() == 1480518) {
            System.out.println("YUHU ONE");
        }
        if (day2.partTwo() == 1282809906) {
            System.out.println("YUHU TWO");
        }
    }

    public int partOne() {
        final var forward = getSumOfValues(FORWARD);
        final var vertical = getSumOfValues(UP) - getSumOfValues(DOWN);
        return Math.abs(vertical * forward);
    }

    public int partTwo() {
        input.forEach(in -> {
            // todo refactor
            if (in.contains(DOWN)) {
                aimDown(in);
            } else if (in.contains(UP)) {
                aimUp(in);
            } else if (in.contains(FORWARD)){
                moveForward(in);
            }
        });
        return horizontal * depth;
    }

    private void aimDown(final @NonNull String in) {
        aim += getIntFromInput(DOWN, in);
    }

    private void aimUp(final @NonNull String in) {
        aim -= getIntFromInput(UP, in);
    }

    private void moveForward(final @NonNull String in) {
        int value = getIntFromInput(FORWARD, in);
        horizontal += value;
        depth += aim * value;
    }

    private int getSumOfValues(final @NonNull String command) {
        return input.stream()
                .filter(in -> in.contains(command))
                .mapToInt(in -> getIntFromInput(command, in))
                .sum();
    }

    private int getIntFromInput(final @NonNull String command, final @NonNull String in) {
        return parseInt(in.replace(command, "").trim());
    }

}
