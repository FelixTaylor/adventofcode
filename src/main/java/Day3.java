import lombok.NonNull;

import java.util.List;
import java.util.function.IntFunction;

import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;

public class Day3 {
    private ZeroOne zeroOnes;
    private static final int CHAR_ZERO = 48;
    private final List<String> input = FileReader.getStringInput(Day3.class, "day3.txt");
    private final IntFunction<String> TO_GAMMA = i -> zeroOnes.zeros[i] > zeroOnes.ones[i] ? "0" : "1";
    private final IntFunction<String> TO_EPSILON = i -> zeroOnes.zeros[i] < zeroOnes.ones[i] ? "0" : "1";

    public static class ZeroOne {
        public int[] zeros;
        public int[] ones;

        public ZeroOne(final int length) {
            this.zeros = new int[length];
            this.ones = new int[length];
        }
    }

    public static void main(final @NonNull String[] args) {
        final var day3 = new Day3();
        if (day3.partOne() == 3549854) {
            System.out.println("YUHU!");
        }
    }

    private long partOne() {
        var length = input.get(0).length();
        this.zeroOnes = new ZeroOne(length);
        this.input.forEach(in -> range(0, length)
                .forEach(i -> increaseZeroOrOne(in, i)));
        return getGammaRate() * getEpsilonRate();
    }

    private void increaseZeroOrOne(final @NonNull String in, final @NonNull int i) {
        if (in.charAt(i) == CHAR_ZERO) {
            zeroOnes.zeros[i]++;
        } else {
            zeroOnes.ones[i]++;
        }
    }

    private long getGammaRate() {
        return getRate(TO_GAMMA);
    }

    private long getEpsilonRate() {
        return getRate(TO_EPSILON);
    }

    private long getRate(final @NonNull IntFunction<String> mapper) {
        final var length = zeroOnes.ones.length;
        final var out = range(0, length)
                .mapToObj(mapper)
                .collect(joining());
        return Long.parseLong(out, 2);
    }


}
