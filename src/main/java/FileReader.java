import java.io.File;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.io.FileUtils.readFileToString;

public class FileReader {

    public static List<Integer> getIntInput(final Class clazz, final String file) {
        try {
            final var classLoader = clazz.getClassLoader();
            final var f = new File(requireNonNull(classLoader.getResource(file)).getFile());
            return stream(readFileToString(f, "UTF-8").split("\\s+"))
                    .mapToInt(Integer::valueOf)
                    .boxed()
                    .toList();
        } catch (IOException e) {
            return emptyList();
        }
    }

    public static List<String> getStringInput(final Class clazz, final String file) {
        try {
            final var classLoader = clazz.getClassLoader();
            final var f = new File(requireNonNull(classLoader.getResource(file)).getFile());
            return stream(readFileToString(f, "UTF-8").split("\r\n"))
                    .toList();
        } catch (IOException e) {
            return emptyList();
        }
    }
}
