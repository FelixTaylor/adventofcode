import java.util.List;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class Day1 {
    private final List<Integer> input = FileReader.getIntInput(Day1.class, "day1.txt");
    final IntPredicate IS_GREATER_THEN_PREVIOUS = i -> input.get(i) > input.get(i - 1);

    public static void main(String[] args) {
        final var day1 = new Day1();
        System.out.println(day1.partOne());
        System.out.println(day1.partTwo());
    }

    public Long partOne() {
        return range(0, input.size()).skip(1)
                .filter(IS_GREATER_THEN_PREVIOUS)
                .count();
    }

    public Long partTwo() {
        final var sums = range(0, input.size()-2)
                .mapToObj(mapping())
                .collect(toList());

        final IntPredicate IS_GREATER_THEN_PREVIOUS  = i -> sums.get(i) < sums.get(i + 1);
        return range(0, sums.size()-1)
                .filter(IS_GREATER_THEN_PREVIOUS)
                .count();
    }

    private IntFunction<Integer> mapping() {
        return i -> input.get(i) + input.get(i + 1) + input.get(i + 2);
    }

}
